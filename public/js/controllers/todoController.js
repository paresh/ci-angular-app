var angular = require('angular');

var mainController =  require('mainController');

module.exports = angular.module('todoController', [])

	// inject the Todo service factory into our controller
	.controller('mainController', mainController).name;
