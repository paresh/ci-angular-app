var angular = require('angular');

var todoController = require('todoController');
var todoService = require('todoService');

angular.module('scotchTodo', [todoController, todoService]);
