var angular = require('angular');
var todos = require('todos');
module.exports = angular.module('todoService', [])

	// super simple service
	// each function returns a promise object
	.factory('Todos',todos).name;
